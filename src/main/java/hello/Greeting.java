package hello;

public class Greeting {

    private final long id;
    private final String content;
	private final String signature;

    public Greeting(long id, String content,String signature) {
        this.id = id;
        this.content = content;
		this.signature=signature;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
	  public String getSignature() {
        return signature;
    }
}
